/** Convert hsl to rgb
 * @param h - hue
 * @param s - saturation
 * @param l - lightness
 */
function hslToRgb(h, s, l) {

  //I don't know what c, x, and m are
  var c = (1 - Math.abs((2 * l) - 1)) * s;
  var x = c * (1 - Math.abs(((h / 60) % 2) - 1));
  var m = l - (c / 2);

  //convert rPrim, gPrime, and bPrime to r, g, b
  var getRgb = function(rPrime, gPrime, bPrime) {
    return {
      R: (rPrime + m) * 255,
      G: (gPrime + m) * 255,
      B: (bPrime + m) * 255
    };
  };


  //different combinations of c, x, and 0 depending on the h (degrees)
  var rgb = new Object();
  if(h >= 0 && h < 60) {
    rgb = getRgb(c, x, 0);
  } else if(h >= 60 && h < 120) {
    rgb = getRgb(x, c, 0);
  } else if(h >= 120 && h < 180) {
    rgb = getRgb(0, c, x);
  } else if(h >= 180 && h < 240) {
    rgb = getRgb(0, x, c);
  } else if(h >= 240 && h < 300) {
    rgb = getRgb(x, 0, c);
  } else if(h >= 300 && h < 360) {
    rgb = getRgb(c, 0, x);
  }

  return rgb;
}

/** getHexFromHue
 * Get the color of Philips Hue light 1 and convert to hex
 * @params lightNum - Philip's Hue light (1-4) to get color from
 * @params hueRotation - how many degrees to offset color circle
 * @params cb - callback 'cb(data)'; data will contain the converted hex color
 */
exports.getHexFromHue = function(lightNum, hueRotation, cb) {
  var json = require('child_process').exec("hue -j lights " + lightNum).stdout.on('data', (data) => {
    var parsed = JSON.parse(data);
    var hue = (((parsed.state.hue / 65535.0) * 360.0) + hueRotation) % 360.0;
    var sat = parsed.state.sat / 254.0;
    var bri = 0.5;

    rgb = hslToRgb(hue, sat, bri);
    hex = (rgb.R << 16 | rgb.G << 8 | rgb.B).toString(16).padStart(6, "0");

    cb(hex);
  });
}
